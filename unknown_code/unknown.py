import csv


def main():
    """

    Reads the CSV file and prints its contents

    """
    path = 'csv_files/'
    fname = 'iris'

    header, rows = read_from_csv(path, fname)

    print(header)

    for row in rows:
        print(row)


def types(row):
    """Get the variable types for each column

    Parameters
    ----------
    column : array like
        The column to obtain the data types

    Returns
    -------
    array
        An array containg the data types for the row

    """

    dtypes = []
    for column in row:
        try:
            int(column)
            dtypes.append('int')
        except:
            try:
                float(column)
                dtypes.append('float')
            except:
                dtypes.append('str')

    return dtypes


def to_int(x):
    """Converts x to an int

    Parameters
    ----------
    x : string
        The string to be converted

    Returns
    -------
    int
        If the cast was succesful, returns the string casted to an int,
        NaN otherwise

    """

    r = 'NaN'
    try:
        r = int(x)
    except:
        pass

    return r


def to_float(x):
    """Converts x to a float

    Parameters
    ----------
    x : string
        The string to be converted

    Returns
    -------
    float
        If the cast was succesful, returns the string casted to a float,
        NaN otherwise

    """

    r = 'NaN'
    try:
        r = float(x)
    except:
        pass

    return r


def format_column(data_type, column):
    """

    Cast the column to its data type

    Parameters
    ----------
    data_type : string
        The data type for the column

    column : string
        The column to be casted

    Returns
    -------
    The column formatted to its data type

    """

    t = column

    if data_type == 'int':
        t = to_int(column)

    elif data_type == 'float':
        t = to_float(column)

    return t


def decode_row(row, types):
    """

    Cast every column of the row to its corresponding data type

    Parameters
    ----------
    row : array
        The row to be decoded

    types : array
        The data types of each column

    Returns
    -------
    array
        Returns the row with every column casted to each correspoding data type

    """

    decoded = []

    for column, data_t in zip(row, types):
        decoded.append(format_column(data_t, column))

    return decoded


def read_from_csv(path, fname):
    """

    Reads the file located at {path}/{fname}.csv and returns the header of the
    CSV file along with the data

    Parameters
    ----------
    path : string
        The path containing the file

    fname : string
        The name of the file without the extension

    Returns
    -------
    Returns a tuple containg the header of the file, and the decoded rows of the
    CSV file

    """

    data = []

    with open('{}/{}.csv'.format(path, fname)) as f:
        csvf = csv.reader(f)

        header = next(csvf, None)
        first_row = next(csvf, None)

        data_types = types(first_row)

    with open('{}/{}.csv'.format(path, fname)) as f:
        csvf = csv.reader(f)
        header = next(csvf, None)

        for row in csvf:
            data.append(decode_row(row, data_types))

    return (header, data)


if __name__ == '__main__':
    main()
